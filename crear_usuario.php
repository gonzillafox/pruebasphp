<?php
include 'regex.php';

if (!array_key_exists('crear', $_POST)) {
    die('Error');
}

if (!regex_dni($_POST['dni'])) {
    die('DNI invalido');
}

$fechaActual = (new \Datetime())->format('Y-m-d H:i:s');

$dbh = new PDO('mysql:host=localhost;dbname=usuarios', 'root', 'c4lpurn14');

$stmt = $dbh->prepare('INSERT INTO TB_usuarios (nombre, password,dni,fecha) VALUES (?, ?, ?,?)');

$stmt->execute([$_POST['nombre'], password_hash($_POST['password'], PASSWORD_DEFAULT),
$_POST['dni'], $fechaActual]);

header('Location: index.php');