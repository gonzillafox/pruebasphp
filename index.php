<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <title><?php echo "Intro PHP" ?></title>
        <link rel="stylesheet" href="public/semantic.min.css">
    </head>
    <body>
        <div class="ui two column stackable aligned grid segment">
          <div class="column">
              <form method="post" action="crear_usuario.php">
                  <div class="ui form segment">
                    <div class="field">
                      <label>Nombre</label>
                      <input placeholder="Nombre de usuario" type="text" name="nombre">
                    </div>
                    <div class="field">
                      <label>DNI</label>
                      <input placeholder="Escriba DNI" type="text" name="dni">
                    </div>
                    <div class="field">
                      <label>Contraseña</label>
                      <input placeholder="Contraseña" type="password" name="password">
                    </div>
                    <input type="submit" class="ui submit button" value="Crear" name="crear">
                </div>
              </form>
          </div>
        </div>
        <script src="public/semantic.min.js"></script>
    </body>
</html>