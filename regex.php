<?php
function regex_dni($dni) {
    return preg_match('/^\d{7,9}$/', $dni);
}